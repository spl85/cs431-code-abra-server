from pathlib import Path
import torch
import nmslib
import numpy as np
from lang_model_utils import load_lm_vocab, Query2Emb
from general_utils import create_nmslib_search_index

nodoc_vecs = np.memmap(filename='data/code2emb/nodoc_vecs_1m.npy', shape=(100000,500), dtype=np.float32, mode='r')
print(nodoc_vecs)
print(type(nodoc_vecs[0][0]))
search_index = create_nmslib_search_index(nodoc_vecs)
search_index.saveIndex('./data/search/search_index.nmslib')