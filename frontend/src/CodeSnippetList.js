import React from 'react';
import styled from 'styled-components';
import {Droppable} from 'react-beautiful-dnd';
import {CodeSnippet} from './Home.js';
import {TabList} from 'react-tabs'
const Title = styled.h3`
	padding: 7px;
`;

const Container = styled.div`
	margin:7px;
	border: 2px solid lightblue;
	border-radius:8px;
`;

const List = styled.div`
	padding:7px;
`;


export class CodeSnippetList extends React.Component{
	render(){
		return(
			<Container>
				<Title>Your search results</Title>
			<Droppable droppableId={""+ this.props.index}>
			{(provided) => (
				<List
					ref={provided.innerRef}
					{...provided.droppableProps}
				>
				
					{this.props.codeSnippets.map((codeSnippet) => <CodeSnippet open={this.props.open} key={codeSnippet.index} index={codeSnippet.index} content={codeSnippet.content} probability={codeSnippet.probability}/>)}
					{provided.placeholder}
				</List>
			)}
			</Droppable>
			</Container>

		);
	}
}