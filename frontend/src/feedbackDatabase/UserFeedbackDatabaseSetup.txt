1. Start up mongodb shell and create a new database called 'ratings'
2. in mongodb shell, enter 'use ratings'
3. cd into the project /src/feedbackDatabase folder
4. in the folder, run 'npm install -g nodemon'
5. then run 'nodemon server' in command prompt and wait for message of connection to feedbackDatabase
6. then run react app and database should be connected

after initial setup
just need to do steps 3 and 5 to start up the connection to the database

Database Schema
database is made up of ratings
each rating consists of
comment: String
likert_rating: number
result_position: number
result_position is the numerical ranking of the returned search result

*to quickly populate the database (will be removed later)
there is a rating submission form at the bottom of the feedback page
comment and ratings can be set, but since result_position depends on code snippets, by default its value is 1 for now
after submission, refresh the page and the charts should update, and you can submit another rating

