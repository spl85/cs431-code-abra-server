const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Rating = new Schema({
    comment: {
        type: String
    },
    likert_rating: {
        type: Number
    },
    result_position: {
        type: Number
    }
});

module.exports = mongoose.model('Rating', Rating);