const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const ratingRoutes = express.Router();
const PORT = 4000;


/*
Code to start up the server to mongodb database
cd into server folder and run nodemon server to create connection to database
*/
let Rating = require('./rating.model');

app.use(cors());
app.use(bodyParser.json());

mongoose.connect('mongodb://127.0.0.1:27017/ratings', { useNewUrlParser: true });
const connection = mongoose.connection;

connection.once('open', function() {
    console.log("MongoDB database connection established successfully");
})

ratingRoutes.route('/').get(function(req, res) {
    Rating.find(function(err, ratings) {
        if (err) {
            console.log(err);
        } else {
            res.json(ratings);
        }
    });
});

ratingRoutes.route('/:id').get(function(req, res) {
    let id = req.params.id;
    Rating.findById(id, function(err, rating) {
        res.json(rating);
    });
});

ratingRoutes.route('/update/:id').post(function(req, res) {
    Rating.findById(req.params.id, function(err, rating) {
        if (!rating)
            res.status(404).send("data is not found");
        else
            rating.comment = req.body.comment;
            rating.likert_rating= req.body.likert_rating;

            rating.save().then(rating => {
                res.json('Rating updated!');
            })
            .catch(err => {
                res.status(400).send("Update not possible");
            });
    });
});

ratingRoutes.route('/add').post(function(req, res) {
    let rating = new Rating(req.body);
    rating.save()
        .then(rating => {
            res.status(200).json({'rating': 'rating added successfully'});
        })
        .catch(err => {
            res.status(400).send('adding new rating failed');
        });
});

ratingRoutes.route('/delete').put(function(req, res){
    Rating.deleteMany({},callback)
      .then(data => {
        res.send({
          message: `${data.deletedCount} Tutorials were deleted successfully!`
        });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all tutorials."
        });
      });
  });

export function clearDatabase() {
    Rating.deleteMany({})
    console.log("deleted")
  }

app.use('/ratings', ratingRoutes);

app.listen(PORT, function() {
    console.log("Server is running on Port: " + PORT);
});