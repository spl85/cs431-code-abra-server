import React, {Component} from 'react';

export class About extends React.Component{
	render(){
		return(
			<React.Fragment>
			<br></br>
			<br></br>
			<br></br>
			<h1>About Code-Abra</h1>
			<p>
				Code-Abra is an open source software designed to help coders speed up the development of their projects by providing a code search functionality at a functional granularity
			</p>
			<br></br>
			<br></br>
			<br></br>
			<h1>How to Query</h1>
			<p>Try typing in a query for code.</p>
			<br></br>
			<p>A typical format of a query will look like this:</p>
			<br></br>
			<p>[your query] [number of snippets you want]</p>
			<br></br>
			<p>Example Query:</p>
			<br></br>
			<p>read wav file 4</p>
			<br></br>
			<p>The number 4 here can be positioned anywhere in the query. If you have multiple integers in your query, we will take the last one as we will presume that the last integer is the number that you truly want.</p>
			<br></br>
			<p>An invalid query will look like this:</p>
			<br></br>
			<p>read wav file 4.76</p>
			<br></br>
			<p>You don't have to tell us how many snippets you want explicitly, and if you decide not to, we'll give you 10</p>
			<br></br>
			<h2>Now that you know the basics, get out there and query!</h2>
			</React.Fragment>
		);
	}
}