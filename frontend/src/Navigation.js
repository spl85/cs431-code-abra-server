import React from 'react';
import {Form,FormControl,Navbar, Nav} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';
import {MDBIcon} from 'mdbreact';

var icon = (
	<MDBIcon icon="magic"/>

);

export class Navigation extends React.Component{
	render(){
		return(
			<Navbar brand={icon} fixed="top" bg="dark" expand="xl">
			<Navbar.Brand><div>{icon}</div><h4 className="heading">Code Abra</h4></Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse aria-controls="basic-navbar-nav"/>
			<Nav>
			<NavLink className="d-inline p-2 m-1 bg-dark text-white" to="/">Home</NavLink>
			<NavLink className="d-inline p-2 m-1 bg-dark text-white" to="/about">About</NavLink>
			<NavLink className="d-inline p-2 m-1 bg-dark text-white" to="/feedback">Feedback</NavLink>
			</Nav>
			
			</Navbar>

		);
	}

}