﻿exports.Query = class Query{
    
    /**
     * Constructs a new Query object which encapsulates the validity of a query
     * @param {*} line the query itself
     * @param {*} lineNum integer value denoting the number of the query (3, 4, etc...)
     * @param {*} resultsReq results requested for the query, -1 if the query is not valid
     * @param {*} isValid boolean denoting the validity of a query
     * @param {*} errorMsg string description of the error if the query is NOT valid; otherwise, value is 'No error'
     */
    constructor(line, lineNum, resultsReq, isValid, errorMsg){
        this.line = line 
        this.lineNum = lineNum
        this.resultsReq = resultsReq
        this.isValid = isValid
        this.errorMsg = errorMsg
    }

    /* self explanatory getters */
    getLine(){return this.line}
    getLineNum(){return this.lineNum}
    getResultsReq(){return this.ResultsReq}
    validity(){return this.isValid}
    getErrorMessage(){return this.errorMsg}
}