exports.CodeSnippet = class CodeSnippet{

/**
     * Constructs a new CodeSnippet object which encapsulates the content of the snippet (the code itself) and the probability of being a good result
     * @param {*} content code snippet string
     * @param {*} probability float indicating the likelihood that the query is what the user wants
     */
    constructor(content, probability){this.content = content, this.probability = probability}

    /* self explanatory getters */
    getContent(){return this.content}
    getProbability(){return this.probability}
}