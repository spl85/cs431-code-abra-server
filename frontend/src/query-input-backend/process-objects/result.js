﻿exports.Result = class Result{
    
    /**
     * Constructs a new Result object which encapsulates the result of a single query
     * @param {*} results an array of 'CodeSnippet' objects; if 0, the query was invalid
     * @param {*} resultsReq integer indicating how many results requested for a particular query
     */
    constructor(results, resultsReq){this.results = results; this.resultsReq = resultsReq}

    /* self explanatory getters */
    getResults(){return this.results}
    getResultsReq(){return this.resultsReq}
}