﻿exports.QueryCheck = class QueryCheck{
    
    /**
     * Constructs a new QueryCheck object which encapsulates the validity of a query
     * @param {*} line the query itself
     * @param {*} lineNum integer value denoting the number of the query (3, 4, etc...)
     * @param {*} isValid boolean denoting the validity of a query
     * @param {*} errorMsg string description of the error if the query is NOT valid; otherwise, value is 'No error'
     */
    constructor(line, lineNum, isValid, errorMsg){this.line = line; this.lineNum = lineNum; this.isValid = isValid; this.errorMsg = errorMsg}

    /* self explanatory getters */
    getLine(){return this.line}
    getLineNum(){return this.lineNum}
    validity(){return this.isValid}
    getErrorMessage(){return this.errorMsg}
}