﻿const ProcessHandler = require('./process-handler/process-handler').ProcessHandler
/*Public QueryInput class. Used as a boundary requestee object by the Controller class to perform query processing.
 * In other words, this class is a driver to the 'querybl' package as a whole.*/
exports.QueryInput = class QueryInput {
    /* constructor */
    constructor(input) {
        this.input = input
    }

    /**
     * Boundary object which passes the input to the processing backend blackbox
     * @returns an array of fully processed queries with results
     */
    processInput() {
        let input = this.input
        let process_handler = new ProcessHandler(input)
        return process_handler.handleProcesses()
    }
}
