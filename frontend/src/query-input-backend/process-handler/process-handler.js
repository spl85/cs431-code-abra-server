﻿const QueryValidator = require('../concrete-processes/query-validator').QueryValidator
const QueryProcessor = require('../concrete-processes/query-processor').QueryProcessor
const EmbeddingComparator = require('../concrete-processes/embedding-comparator').EmbeddingComparator

exports.ProcessHandler = class ProcessHandler{

    constructor(input){this.input = input}

    handleProcesses(){
        //process chain
        return new EmbeddingComparator(new QueryProcessor(new QueryValidator(this.input).process()).process()).process()
    }
}