/**
*Script that does all the work necessary to setup a server for the
*frontend and to connect to the search index library 
*/

//establish this as a server for frontend
const express = require("express");
const http = require("http");
const socketIo = require("socket.io");

const port = process.env.PORT || 4001;
const index = require("./routes/index");

const app = express();
app.use(index);

const server = http.createServer(app);
const io = socketIo(server);

io.on("connection", (socket) => {
  console.log("New client connected");
  socket.on("disconnect", () => {
    console.log("Client disconnected");
  });

  socket.on('message', (query) => {
      processQuery(socket, query)
    })
});

server.listen(port, () => console.log(`Listening on port ${port}`));

/**
 * Sets up the connection to the search index server running on port 80 to run queries against the index and return results
 * @param {String} query the full query string to be processed on the search index server
 */
function processQuery(socket, query){
      //establish connection to search index server
      const net = require('net')
      const QueryInput = require('./query-input').QueryInput
      let results = new Array()
      let snippets = new Array()
      let queries = new Array()
      const soc = net.connect({port:80, host:"192.168.99.100"})
      let i = 0
      let codeSnippet = {}
      let content = true
      let snippetLength = 0
	  let index = 0;
	  
      soc.on('ready', ()=>{
          let qi = new QueryInput(query)
          queries = qi.processInput()
          //write first query
          soc.write(JSON.stringify(queries[i]))})
      soc.on('data', (data) => {
          data = data.toString('utf-8')
          if(!isNaN(parseInt(data)) && Object.keys(codeSnippet).length === 0 && codeSnippet.constructor === Object){
              //100% this is the length of the results
              snippetLength = parseInt(data)
          } else if(data  === 'end_of_query'){
              //the data marks the end of a query result
              //attach and reset current snippets list to results array
              results = results.concat({snippets: snippets, query: queries[i]})
              snippets = new Array()
              //send next query
              ++i
              if(i < queries.length){
                 soc.write(JSON.stringify(queries[i]))
                  return
              } else{
                  soc.write('quit_code_359')
                  return
              }
          } else if(data === 'invalid'){
                soc.write('received')
                return
          } else {
              //either a probabilty or content
              if(isNaN(parseInt(data))){
                  //content
                  if(codeSnippet.content === undefined) codeSnippet.content = data
                  else codeSnippet.content = codeSnippet.content + data
                  snippetLength -= data.length
                  if(snippetLength !== 0){
                      //we are not done with the content and can not write 'received'
                      return
                  } else{
                      //we are done with the content, can write 'received' and reset snippetLength
                      snippetLength = 0
                  }
              } else{
                  //probability, add snippet to snippets array, and reset snippet object
                  codeSnippet.probability = data
				  codeSnippet.index = index++;
                  snippets = snippets.concat(codeSnippet)
                  codeSnippet = {}
              }
              
          }
          soc.write('received')
      })
      
      //print results on socket closure if debugging (results will be fully populated)
      soc.on('end', () => {
          socket.emit('results', results)
          soc.destroy()
      })
}

/**
 * Used to print the results in a formatted way (mainly used for debugging)
 * @param {2D Array} results results of a batch of queries
 */
function formattedPrint(results){
    for(let n = 0; n < results.length; n++){
        console.log('Results for Query: ' + queries[n].line + '\n')
        for(let q = 0; q < results[n].length; q++){
            console.log(q + 1 + ': distance: ' + results[n][q].probability + ' ' + results[n][q].content)
        }
        console.log('\n')
    }
}