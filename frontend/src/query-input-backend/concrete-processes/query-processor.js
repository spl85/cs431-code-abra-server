﻿const QueryCheck = require('../process-objects/query-check').QueryCheck
const Query = require('../process-objects/query').Query

exports.QueryProcessor = class QueryProcessor {

    /**
     * Constructs a new 'QueryProcessor' object
     * @param {*} input array containing 'QueryCheck' objects from the previous processing step
     */
    constructor(input){this.input = input}

    /**
     * Processes an expected array of 'QueryCheck' objects into an array of 'Query' objects.
     * @returns 'queries', an array of 'Query' objects
     */
    process(){
        let input = this.input
        let queries = Array()
        input.forEach(query_check => {
            if(query_check.validity()){
                let results_req = query_check.getLine().match(/\d+/g)
                if(results_req == null){results_req = 10}
                //take the last number we find as the true requested number of results, if greater than 100, invalid.
                else if(results_req > 100){
                    let query = new Query(query_check.getLine(), query_check.getLineNum(), -1, false, 'The requested number of results must be 100 or below')
                    queries = queries.concat(query)
                    return
                }
                else{results_req = Number(results_req[results_req.length-1])}
                let query = new Query(query_check.getLine(), query_check.getLineNum(), results_req, query_check.validity(), query_check.getErrorMessage())
                queries = queries.concat(query)
            } else{
                let query = new Query(query_check.getLine(), query_check.getLineNum(), -1, query_check.validity(), query_check.getErrorMessage())
                queries = queries.concat(query)
            }
        })
        return queries
    }

    /**
     * Sets this object's input as the input passed in the parameter
     * @param {*} input this object's desired input string to process
     */
    setInput(input){
        this.input = input
    }
}