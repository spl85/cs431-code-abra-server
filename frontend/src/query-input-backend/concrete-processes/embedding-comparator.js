﻿const Query = require('../process-objects/query').Query
const Result = require('../process-objects/result').Result
const CodeSnippet = require('../process-objects/code-snippet').CodeSnippet
exports.EmbeddingComparator = class EmbeddingComparator {

    /**
     * Constructs a new 'EmbeddingComparator' object which opens a connection to run queries against the search index running on the server
     * @param {*} input array of 'Query' objects that can be either valid or invalid; only valid queries are sent to the server while others are thrown into the resuls as is
     */
    constructor(input) {this.input = input}

    process(){
        //return input (queries)
        return this.input
    }

    /**
     * Sets this object's input as the input passed in the parameter
     * @param {*} input this object's desired input string to process
     */
    setInput(input){this.input = input}
}