﻿const QueryCheck = require('../process-objects/query-check').QueryCheck
exports.QueryValidator = class QueryValidator {

    /**
     * Constructs a QueryValidator object with a starting input to process
     * @param {*} input The total input string the user typed into the text box
     */
    constructor(input) {
        this.input = input
    }

    /**
     * Processes input into an array of single queries that are either deemed valid or non-valid based on 2 high-level factors
     * @returns An array of 'QueryCheck' objects containing an 'isValid' boolean and an error message for a non-valid query among other items
     */
    process() {
        let input = this.input
        //seperate the input into queries delimited with '.' and '\n' -- REQ-3a
        input.trim()
        if(input[input.length-1] === '.' || input[input.length-1] === '\n') {input = input.substring(0, input.length-1)}
        let queries = input.split(/\.[^0-9+]/g)
        let clean_queries = Array()
        queries.forEach(query => {clean_queries = clean_queries.concat(query.trim().split('\n'))})
        
        //restriction on number of queries is 20
        if(clean_queries.length > 20) return Array()

        //solution array containing QueryCheck objects to be sent to next process
        let query_checkers = Array()

        //for each clean query, find floats or non-alphanumeric characters; if either exists in the query, it is malformed, otherwise it is well-formed
        let i = 0
        clean_queries.forEach(query => {
            ++i
            query = query.trim()
            if(query.match(/\d+\.\d+/g) != null){
                //invalid query
                let qc = new QueryCheck(query, i, false, 'We cannot give you ' + query.match(/\d+\.\d+/g) + ' of this query')
                query_checkers = query_checkers.concat(qc)
            } else if(query.match(/\W+/g) != null){
                if(query.match(/\W+/g).filter((v, i, test) => {v != ' ' || v != '\n'}).length != 0){
                    //invalid query
                    let qc = new QueryCheck(query, i, false, 'Contains one or more invalid characters')
                    query_checkers = query_checkers.concat(qc)
                } else{
                    //valid
                    let qc = new QueryCheck(query, i, true, 'No error')
                    query_checkers = query_checkers.concat(qc)
                }
            } else{
                if(query.length <= 1) {
                    //invalid
                    let qc = new QueryCheck(query, i, false, 'Query is malformed')
                    query_checkers = query_checkers.concat(qc)
                } else{
                    //valid query
                    let qc = new QueryCheck(query, i, true, 'No error')
                    query_checkers = query_checkers.concat(qc)
                }
            }
        })
        return query_checkers
    }

    /**
     * Sets this object's input as the input passed in the parameter
     * @param {*} input this object's desired input string to process
     */
    setInput(input){
        this.input = input
    }
}