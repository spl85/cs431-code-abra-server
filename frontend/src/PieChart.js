import React from "react";
import { Pie } from "react-chartjs-2";
import { MDBContainer } from "mdbreact";

class PieChart extends React.Component {
  state = {
    dataPie: {
      labels: ["Disagree","Agree","Neither Agree/Disagree"],
      datasets: [
        {
          data: this.props.data,
          backgroundColor: [
            "#F7464A",
            "#46BFBD",
            "#FDB45C",
            "#949FB1",
            "#4D5360",
            "#AC64AD"
          ],
          hoverBackgroundColor: [
            "#FF5A5E",
            "#5AD3D1",
            "#FFC870",
            "#A8B3C5",
            "#616774",
            "#DA92DB"
          ]
        }
      ]
    }
  }
  componentDidMount(){
    //console.log("pie chart mounted")
    console.log(this.props.data)
  }

  // componentDidUpdate(prevProps) {
  //   // Typical usage (don't forget to compare props):
  //   console.log("props check for pie chart")
  //   if (this.props.data!== prevProps.data) {
  //     //this.setState({ data: { ...this.state.dataPie.datasets,{data:this.props.data}});
  //     this.setState(this.state => (this.state.dataPie.datasets.data = this.props.data, state))
  //     console.log(this.state.dataPie.datasets.data)
  //     pieData=this.props.data
  //     console.log(this.props.data)
  //   }
  // }
  render() {
    return (
      <MDBContainer>
        <h3 className="mt-5">Pie Chart showing the Satisfaction of Returned Results</h3>
        <br></br>
        <Pie data={this.state.dataPie} options={{ responsive: true }} />
      </MDBContainer>
    );
  }
}

export default PieChart;