import React from 'react';
import { MDBContainer, MDBRow, MDBCol } from 'mdbreact';
import axios from 'axios';
import StarRatingComponent from 'react-star-rating-component';

class Rating_Form extends React.Component{

    //need 3 components
    //one text field for comment
    //radio buttons or dropdown menu for likert_rating
    //submit button-> send to database
    constructor(props) {
        super(props);

        this.onChangeComment = this.onChangeComment.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            comment: '',
            likert_rating: 0,
            result_position: this.props.position+1, //this should be passed in via props
            disabledButton:true,
            permanentDisable:false,
            buttonMessage:"Submit",
        }
    }

    onChangeComment(e) {
        this.setState({
            comment: e.target.value,
        });
    }


    onStarClick(next,prev,name){
        if(this.state.permanentDisable){
            this.setState({
                likert_rating: next,
                disabledButton:true
            });
        }else{
            this.setState({
                likert_rating: next,
                disabledButton:false
            });
        }
	}


onSubmit(e) {
    e.preventDefault();
    
    console.log(`Form submitted:`);
    console.log(`Comment: ${this.state.comment}`);

    console.log(`Likert Rating: ${this.state.likert_rating}`); 

    const newRating = {
        comment: this.state.comment,
        likert_rating: this.state.likert_rating,
        result_position: this.state.result_position,
    };
    axios.post('http://localhost:4000/ratings/add', newRating)
        .then(res => console.log(res.data));
    
    this.setState({ //after submission, submit button should be disabled
        disabledButton:true,
        permanentDisable:true,
        buttonMessage:"Submitted",
        open: false

    })
}


componentDidUpdate(prevProps) {

    // console.log("update check")
    // console.log(this.props.open+ " "+prevProps.open)
    if (this.props.open !== prevProps.open) {
        // console.log("new query")
       this.setState({
            comment: '',
            likert_rating: 0,
            result_position: this.props.position+1, //this should be passed in via props
            disabledButton:true,
            permanentDisable:false,
            buttonMessage:"Submit",
        });
    }
  }


render() {
    return (
        <div class="container">
            <div class= "row">
            <div style={{marginTop: 10}}>
            <h4>Rating Submission</h4>
            <form onSubmit={this.onSubmit}>
                <div class="col-sm">
                <div className="form-group"> 
                    <label>Comment: </label>
                    <input  type="text"
                            className="form-control"
                            value={this.state.comment}
                            onChange={this.onChangeComment}
                            />
                </div>
                </div>
               <div class="col-sm">
               <h4>Likert Rating</h4>
							<h4><StarRatingComponent starColor="#c2eceb" starCount={5} value={this.state.likert_rating} onStarClick={this.onStarClick.bind(this)}/></h4>
                
                </div>
                <div class='col-sm'>
                <div className="form-group">
                    <input type="submit" value={this.state.buttonMessage} className="btn btn-primary" disabled={this.state.disabledButton} />
                </div>
                </div>
            </form>
            <div></div>

        </div>
        </div>
        </div>
    )
}


}
export default Rating_Form