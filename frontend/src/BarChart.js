// BarChart.js
import React from 'react';
import { HorizontalBar } from 'react-chartjs-2';
import { MDBContainer } from 'mdbreact';

var all_labels=["Top 1", "Top 2", "Top 3", "Top 4","Top 5","Top 6", "Top 7","Top 8", "Top 9","Top 10",
  "Top 11", "Top 12", "Top 13", "Top 14","Top 15","Top 16", "Top 17","Top 18", "Top 19","Top 20"]

class BarChart extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      dataHorizontal: {
        labels: all_labels.slice(0,this.props.k),
        datasets: [
          {
            label: 'Number of Results',
            data:this.props.data,
            fill: false,
            backgroundColor: [
              'rgba(225,87,255,0.2)',
              'rgba(232,77,72,0.2)',
              'rgba(255,93,155,0.2)',
              'rgba(232,72,213,0.2)',
              'rgba(218,79,255,0.2)',
              'rgba(196,87,255,0.2)',
              'rgba(140,93,232,0.2)',
              'rgba(120,115,255,0.2)',
              'rgba(93,130,232,,0.2)',
              'rgba(102,188,255,0.2)',
              'rgba(82,222,255,0.2)',
              'rgba(88,232,216,0.2)',
              'rgba(109,255,191,0.2)',
              'rgba(88,232,124,0.2)',
              'rgba(111,255,97,0.2)',
              'rgba(255,249,97,0.2)',
              'rgba(232,212,102,0.2)',
              'rgba(255,220,125,0.2)',
              'rgba(232,183,102,0.2)',
              'rgba(255,180,112,0.2)'
            ],
            borderColor: [
              'rgb(225,87,255)',
              'rgb(232,77,72)',
              'rgb(255,93,155)',
              'rgb(232,72,213)',
              'rgb(218,79,255)',
              'rgb(196,87,255)',
              'rgb(140,93,232)',
              'rgb(120,115,255)',
              'rgb(93,130,232,)',
              'rgb(102,188,255)',
              'rgb(82,222,255)',
              'rgb(88,232,216)',
              'rgb(109,255,191)',
              'rgb(88,232,124)',
              'rgb(111,255,97)',
              'rgb(255,249,97)',
              'rgb(232,212,102)',
              'rgb(255,220,125)',
              'rgb(232,183,102)',
              'rgb(255,180,112)'
            ],
            borderWidth: 1
          }
        ]
      }
    }
      //this.handleChange = this.handleChange.bind(this);
    }
    // shouldComponentUpdate(nextProps) {
    //   console.log(nextProps !== this.props)
    //   return nextProps !== this.props
    // }

    componentDidUpdate(prevProps) {
      // Typical usage (don't forget to compare props):
      //console.log("props check")
      if (this.props.k !== prevProps.k) {
        this.setState({ dataHorizontal: { ...this.state.dataHorizontal, labels: all_labels.slice(0,this.props.k)} });
        //console.log("state changed")
      }
    }

  //   get_K=(data_from_k_selector)=>{
  //     var k_data=data_from_k_selector;
  //     console.log("k received to be "+k_data)
  //     this.setState({k:k_data});
  // }
  render() {
    return (
      <div className="BarChart">
        <MDBContainer>
        <h3 className='mt-5'>Distribution of Rankings of the Returned Results</h3>
        <HorizontalBar
          data={this.state.dataHorizontal}
          options={{ responsive: true }}
        />
      </MDBContainer>
      
      
      </div>
    );
  }
}

export default BarChart;
