import React, { Component } from 'react';
import './App.css';
import BarChart from './BarChart';
import PieChart from './PieChart'
import {MDBCard, MDBCardTitle, MDBCardText, MDBContainer} from "mdbreact";
import K_Selector from './K_Selector'
import axios from 'axios';
import Rating_Form from "./Rating_Form"



var counts=[0,0,0]
var totalNum=0;
var barChartCounts=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
export class Feedback extends React.Component{
    constructor(props){
      super(props)
      this.state= {ratings:[], k:5, isLoading:true, clearButton:false}
    }
componentDidMount() {
  axios.get('http://localhost:4000/ratings/')
      .then(response => {
          const ratings=response.data;
          console.log("got data successfully")
          this.setState({ratings:ratings});
          this.getPieChartCounts()
          this.getBarChartCounts()
          console.log("data set")
          this.setState({isLoading:false})
      })
      .catch(function (error){
          console.log(error);
      })
 }

 get_K=(data_from_k_selector)=>{
    var k_data=data_from_k_selector;
    console.log("k received to be "+k_data)
    this.setState({k:k_data});
}
clearDatabase(){
  var data=this.state.ratings
    data.forEach(function(item) {
    var target=item._id;
    //console.log(target)
    axios.delete('http://localhost:4000/ratings/delete/'+target)
    });

}


handleClick() {
  this.clearDatabase()
  this.setState({
    clearButton: true,
});
}

 getPieChartCounts(){
    var data=this.state.ratings
    data.forEach(function(item) {
    var number= item.likert_rating;
    if(number<=2 && number>0){
      counts[0]++
    }else if(number===3){
      counts[2]++
    }else if(number>=4){
      counts[1]++
    }else{

    }
    //console.log("counts "+pieCounts)
    });
    
    //console.log("pie chart counts "+counts)
    return 
 }
 getBarChartCounts(){
    var data=this.state.ratings
    data.forEach(function(item) {
    var index= item.result_position;
    if(index-1<20 &&index!==0){
        barChartCounts[index-1]++
        //console.log(barChartCounts)
    }
    totalNum++
    });
    //console.log("bar chart counts "+barChartCounts)
    return
 }

    render(){
      if(this.state.isLoading===true){
          return <div>Loading data...</div>
      }else{
        return(
       
          <div className="Feedback">
            	<br></br>
              <br></br>
              <MDBContainer size='md'>      
                <BarChart data={barChartCounts}  k={this.state.k}></BarChart>
              </MDBContainer>
            <MDBContainer size='sm'>
              <K_Selector functionCallFromFeedback={this.get_K.bind(this)}/>
            </MDBContainer>
            <MDBContainer size='md'>
              <PieChart data={counts}></PieChart>
            </MDBContainer>

            <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
              <button onClick={() => this.handleClick()} disabled={this.state.clearButton}>
          Clear Database
          </button>

          </div>

          
          </div>
          
        );
      }
    }
  }

/* import React, { Component } from 'react';
import './App.css';
import BarChart from './BarChart';
import PieChart from './PieChart'
import {MDBCard, MDBCardTitle, MDBCardText, MDBContainer} from "mdbreact";
import K_Selector from './K_Selector'
import axios from 'axios';
import Rating_Form from './Rating_Form';




//var counts=[0,0,0]
var totalNum=0;
//var barChartCounts=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
var all_labels=["Top 1", "Top 2", "Top 3", "Top 4","Top 5","Top 6", "Top 7","Top 8", "Top 9","Top 10",
  "Top 11", "Top 12", "Top 13", "Top 14","Top 15","Top 16", "Top 17","Top 18", "Top 19","Top 20"]


//placeholder data for when database is not connected
var counts=[12,43,22]
var barChartCounts=[3,4,8,12,2]

export class Feedback extends React.Component{
    state= {ratings:[], k:5}
// componentDidMount() { //connects to database
//   axios.get('http://localhost:4000/ratings/')
//       .then(response => {
//           const ratings=response.data;
//           console.log("got data successfully")
//           this.setState({ratings});
//           this.getPieChartCounts()
//           this.getBarChartCounts()
//       })
//       .catch(function (error){
//           console.log(error);
//       })
//  }

 get_K=(data_from_k_selector)=>{
    var k_data=data_from_k_selector;
    console.log("k received to be "+k_data)
    this.setState({k:k_data});
}

 getPieChartCounts(){
    var data=this.state.ratings
    data.forEach(function(item) {
    var number= item.likert_rating;
    if(number<=2){
        counts[0]++
    }else if(number==3){
        counts[2]++
    }else{
        counts[1]++
    }
    console.log("counts "+counts)
    });
    console.log("testing")
    return 
 }
 getBarChartCounts(){
    var data=this.state.ratings
    data.forEach(function(item) {
    var index= item.result_position;
    if(index-1<20){
        barChartCounts[index-1]++
        console.log(barChartCounts)
    }
    totalNum++
    });
    console.log("bar chart counts "+barChartCounts)
    return
 }



    render(){
      return(
       
        <div className="Feedback">
			<br></br>
			<br></br>
          <MDBContainer size='sm'>
            <BarChart data={barChartCounts} labels={all_labels} k={this.state.k}></BarChart>
          </MDBContainer>
          <MDBContainer size='md'>
		    <br></br>
            <PieChart data={counts}></PieChart>
          </MDBContainer>
          <K_Selector functionCallFromFeedback={this.get_K.bind(this)}></K_Selector>

        <Rating_Form></Rating_Form>
  
        </div>
        
      );
    }
  } */