import React, {Component} from 'react';
import {Button} from 'react-bootstrap';

export class CommentBox extends React.Component{
	render(){
		return(
			<React.Fragment>
			<div className="floatright">
			<h6><strong>Comment:</strong></h6>
			<form>
				<textarea rows="3" cols="70" placeholder="Post your comment here..."/>
				<div>
				<Button variant="primary">Post Comment</Button>
				</div>
			</form>
			</div>
			</React.Fragment>
		);
	}
}