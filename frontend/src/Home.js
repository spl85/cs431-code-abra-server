import React, {Component} from 'react';
import {About} from './About.js'
import {MDBCard, MDBCardTitle, MDBCardText, MDBContainer, MDBCardHeader, MDBIcon} from "mdbreact";
import {Button, Form, FormControl,Navbar, Nav,InputGroup} from 'react-bootstrap';
import {NavLink,Route,Switch,BrowserRouter as Router } from 'react-router-dom';
import logo from './logo.svg';
import {Navigation} from './Navigation.js';
import {Draggable,DragDropContext,Droppable} from 'react-beautiful-dnd';
import {CodeSnippetList} from './CodeSnippetList.js';
import './App.css';
import styled from 'styled-components';
import socketIOClient from "socket.io-client";
import {Tabs,Tab,TabList,TabPanel} from 'react-tabs';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCode, faSearch} from '@fortawesome/free-solid-svg-icons'
import StarRatingComponent from 'react-star-rating-component';
import 'react-tabs/style/react-tabs.css';
import {CommentBox} from './CommentBox.js';
import Rating_Form from './Rating_Form'

const ENDPOINT = "http://localhost:4001"
const soc = socketIOClient(ENDPOINT)


const Container = styled.div`
	border: 1px solid lightblue;
	border-radius: 2px;
	padding:7px;
	margin-bottom:8px;
`;



export class CodeSnippet extends React.Component{

	constructor(props){
		super(props);
		this.state = {
			rating: 0,
			thumbsUp : 0, // The state shows mutability ... and we can either thumbs up or thumbs down the code snippet.
			position:this.props.index,
			open:true
		}

	}

	onStarClick(next,prev,name){
		this.setState({rating: next});
	}
	componentDidUpdate(prevProps){
		if (this.props.content !== prevProps.content) {
			this.setState({open:!this.state.open})
			// console.log("different data  "+this.state.open)
		  }
		
	}
	render(){
		return(	
			<React.Fragment>
				<Draggable draggableId={"" + this.props.index} index={this.props.index}>
				{provided => (
				<Container
				{...provided.draggableProps}
				{...provided.dragHandleProps}
				ref={provided.innerRef}
				>
				
				<MDBContainer>
					
					<MDBCard className="card-body">
						<MDBCardTitle style={{textAlign:"center"}}>{this.props.index+1}</MDBCardTitle>
						
							<MDBCardText>
							<p><strong>Content: </strong></p>
							<div style={{backgroundColor:"#c2eceb"}}>{this.props.content.split("\n").map((content) => <div>{content.replace(/ /g, "\u00a0")}</div>)}</div>
							<br/>
							<p><strong>Estimated Probability: </strong> <text>{this.props.probability}</text></p>
							{/* <InputGroup>
							<p><strong>Estimated Probability: </strong> <text>{this.props.probability}</text></p>
							&nbsp;
							&nbsp;
							<CommentBox/>
							&nbsp;
							&nbsp;
							<h4><strong>Rating:</strong></h4>
							<h4><StarRatingComponent starColor="blue" starCount={5} value={this.state.rating} onStarClick={this.onStarClick.bind(this)}/></h4>
							</InputGroup> */}
							<Rating_Form open={this.state.open}position={this.props.index}></Rating_Form>
							
							 
							</MDBCardText>
					</MDBCard>
					
				</MDBContainer>
				
				</Container>
				)}
				</Draggable>
			</React.Fragment>
		);
	}
	
}

class CodeSearcher extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			value: '',
			isClicked: false,
			codeSnippets: [],
			results: []
		}
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.sockethandler();
	}

	sockethandler(){
				soc.on('results', (data) => {
			this.setState({
 				results: data,
				isClicked: true,
			})
			console.log(this.state);
			
		});
	}

	handleChange(event){
		if(event.target.files != null){
			var file = event.target.files[0]
			if(!file) return
			var reader = new FileReader()
			reader.onload = ()=>{this.setState({value:reader.result})}
			reader.readAsText(file)
			return
		}
		this.setState({
			value:event.target.value
		});
	}

	handleSubmit = (event) => {
		event.preventDefault();
		soc.emit('message', this.state.value)
		
		
	}
	
	onDragEnd = result =>{
		const{ destination, source, draggableId } = result;
		if(!destination){
			return;
		}
		if(destination.droppableId === source.droppableId && destination.index === source.index){
			return;
		}
		// Save the source index
		 const temp = this.state.results[source.droppableId].snippets[source.index];
		 this.state.results[source.droppableId].snippets[source.index] = this.state.results[destination.droppableId].snippets[destination.index];
		 this.state.results[destination.droppableId].snippets[destination.index] = temp;
	
		

	};



	render(){
	
		return(
		/* This is the code searcher template */
			<React.Fragment>
				<MDBContainer>
					<div className="searcher">
					<MDBCard className="card-body" style={{textAlign:"center", width: "50rem", margin:"auto", backgroundColor:"#c2eceb"}}>
						<MDBCardTitle> {this.props.title} </MDBCardTitle>
							<MDBCardText>
								<div className="formStyle">
								<form onSubmit={this.handleSubmit} style={{textAlign:"center"}} sinline="true">
								<div className="input-group">
								<Form.Group className="form-group" ontrolId="exampleForm.ControlTextarea1">
								<InputGroup>
								                     
                            <InputGroup.Prepend>
                                <InputGroup.Text>
                                    <FontAwesomeIcon icon={faCode} />
                                </InputGroup.Text>
                            </InputGroup.Prepend>

    							<Form.Control as="textarea" placeholder={this.props.text} value={this.state.value} rows="3" onChange={this.handleChange} />
									<Button type="submit" id="search_button" style={{backgroundColor:"lightblue"}} variant="outline-info">
									<div><FontAwesomeIcon icon={faSearch} /></div>
									</Button>
									</InputGroup>
									<input type="file" id="choose" name="file" accept="text/plain" onChange={this.handleChange}/>
									</Form.Group>
								</div>
								</form>
								</div>
								
										
							</MDBCardText>
					</MDBCard>
						</div>
				</MDBContainer>
				<DragDropContext onDragEnd={this.onDragEnd}>
				
				{this.state.isClicked ? <Tabs><TabList className="tabStyle">{this.state.results.map((result, index) => <Tab>{result.query.line}</Tab>)}</TabList>
				{this.state.results.map((result,index) => <TabPanel><CodeSnippetList codeSnippets={result.snippets} index={index} open={true}/></TabPanel>)}</Tabs> : null}
				
				</DragDropContext>
							
			</React.Fragment>

		);
	}
}

export class Home extends React.Component{	
	render(){
	const snippetName = "Code Searcher";
	const snippetText = "Put in your query here...";
		return (
			<div className="Website">
					
				<br></br>
				<br></br>
				<h5>
					<img src={logo} className="App-logo" alt="logo" />
				</h5>
				<CodeSearcher title={snippetName} text={snippetText}/>
				<br></br>
			</div>
		);
	}
}


