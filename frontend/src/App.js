import React, {Component} from 'react';
import {NavLink,Route,Switch,BrowserRouter as Router } from 'react-router-dom';
import {Button, Form, FormControl,Navbar, Nav} from 'react-bootstrap';
import {Navigation} from './Navigation.js';
import {About} from './About.js';
import {Home} from './Home.js';
import {Feedback} from './Feedback.js';

export default class App extends React.Component{
	render(){
		return(
			<Router>
				<Navigation/>
				<Switch>
					<Route path='/about' component={About}/>
					<Route path='/Feedback' component={Feedback}/>
					<Route path='/' component={Home}/>
				</Switch>
			</Router>
		);
	}
}