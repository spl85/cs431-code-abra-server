This is the project set up for the Code-Abra Code Search Engine. Please carefully read and follow the instructions since there are a lot of components to set up for the project to run. Code-Abra requires that MongoDB, NodeJS, React be already installed on your machine. Docker does not have to be installed yet, and the initial Docker setup will cover what is needed in the Docker installation. Additionally, since you will need several command prompts running, the instructions have been group according to command prompt. 

Project First Time Setup

Stage 1: Docker Setup
1. download docker for windows toolbox (including 'Oracle VM VirtualBox', and kitematic) 
2. open kitematic app
3. click 'docker cli' button in the bottom left corner, which will open specialized command prompt

Stage 2: Creating Container and starting the backend query server
Docker CLI command prompt
4. type ".\docker run -it --name SIServer -p 80:80 -p 7654:7654 hamelsmu/ml-cpu" - this will pull an image which could take some time
5. once you pull the image, go back to kitematic and click on the running container's 'ds' folder located on the righthand side
6. click "enable volumes"
7. move the cs431-code-abra-server repo into the ds folder so that it becomes ../ds/cs431-code-abra-server
8. cd into cs431-code-abra-server
9. type 'pip install tensorflow' to install tensorflow dependencies needed to run the project
10. to start the backend query server, type 'python query.py 80
11. if set up correctly, then you should get the message "MSG: search index server running on this machine at port#: 80"

Stage 3: Connecting Backend Query Server running on Docker to the React Application
New Command Prompt (regular cmd)
12. open a new command prompt and cd to wherever the ds folder containing the project is located
10. cd into 'cs431-code-abra-server/frontend/src/query-input-backend' open the 'middle-man.js' for editing
11. scroll down to the 'processQuery(socket, query)' function and look for the line 'const soc = ...'
12. replace the host IP address with the IP address of the linux virtual machine running your docker container.
	You can find the IP by opening the Oracle VM VirtualBox program and typing 'ip address' inside the virtual machine
13. save the file, and back on the command line terminal type 'node middle-man.js'

Stage 4: Set up User Feedback Database using MongoDB
New Command Prompt (regular cmd)
1. Have mongodb installed
2. To run mongodb using a command prompt, cd into the path where mongodb is installed and then cd into Server\4.2\bin, once inside bin folder run 'mongod' command
3. Start up mongodb shell and create a new database called 'ratings'
4. in mongodb shell, enter 'use ratings'
5. Create a new command prompt, and cd into the project ds/cs431-code-abra-server/src/feedbackDatabase 
6. in the feedbackDatabase folder, run 'npm install -g nodemon'
7. then run 'nodemon server' in command prompt and wait for message of connection to feedbackDatabase

Stage 5: Running the Application
New Command Prompt (regular cmd)
1. Navigate into the 'ds/cs431-code-abra-server/frontend' folder
2. type 'npm start' which will compile and start the application
3. If application doesn't compile and says dependencies are missing, try the 'npm install' command before running 'npm start'
4. If dependencies aren't installed using step 3, try using 'npm install name_of_the_missing_dependency'

Subsequent Project Setup (running project after inital setup)
1. In the Kitematic docker cli command prompt type ' ./docker exec -it SIServer bash'
2. Then cd into the cs431-code-abra-server folder, and run 'python query.py 80' in the docker cli command prompt
3. Open a regular command prompt and cd into where ../ds/cs431-code-abra-server/frontend/src/query-input-backend is located
4. Run 'node middle-man.js'
5. To setup the user feedback database and connection, run steps 2,5,7 in Stage 4)
6. Run all of Stage 5
