import warnings
warnings.filterwarnings('ignore')
from pathlib import Path
import numpy as np
import pandas as pd
import torch
torch.nn.Module.dump_patches = True
import nmslib
from lang_model_utils import load_lm_vocab, Query2Emb
from general_utils import create_nmslib_search_index
import os, sys
import socket as soc
import json

if(len(sys.argv) != 2):
	print('Please provide an exposed port number for the server to bind to\nInvoke like so: python query.py [exposed port]')
	sys.exit()

input_path = Path('./data/processed_data/')
code2emb_path = Path('./data/code2emb/')
output_path = Path('./data/search')
output_path.mkdir(exist_ok=True)

# read original code (capped to 100,000 code snippets for memory resource reasons)
code_df = pd.read_json(input_path/'without_docstrings_original_function.json.gz')
code_df = code_df[:100000]
code_df.columns = ['code']

#language model
lang_model = torch.load('./data/lang_model/lang_model_cpu_v2.torch', map_location=lambda storage, loc: storage)
vocab = load_lm_vocab('./data/lang_model/vocab_v2.cls')
q2emb = Query2Emb(lang_model = lang_model.cpu(), vocab = vocab)
#initialize search index
search_index = nmslib.init(method='hnsw', space='cosinesimil')
search_index.loadIndex('./data/search/search_index.nmslib')

class search_engine:
    """Organizes all the necessary elements we need to make a search engine."""
    def __init__(self, 
                 nmslib_index, 
                 ref_df, 
                 query2emb_func):
        """
        Parameters
        ==========
        nmslib_index : nmslib object
            This is pre-computed search index.
        ref_df : pandas.DataFrame
            This dataframe contains meta-data for search results, 
            must contain the columns 'code' and 'url'.
        query2emb_func : callable
            This is a function that takes as input a string and returns a vector
            that is in the same vector space as what is loaded into the search index.

        """
        
        self.search_index = nmslib_index
        self.ref_df = ref_df
        self.query2emb_func = query2emb_func
    
    def search(self, str_search, csoc, k=2):
        """
        Prints the code that are the nearest neighbors (by cosine distance)
        to the search query.
        
        Parameters
        ==========
        str_search : str
            a search query.  Ex: "read data into pandas dataframe"
        csoc : Socket
            the client socket
        k : int
            the number of nearest neighbors to return.  Defaults to 2.
    
        """
        query = self.query2emb_func(str_search)
        idxs, dists = self.search_index.knnQuery(query, k=k)
        for idx, dist in zip(idxs, dists):
            code = self.ref_df.iloc[idx].code
            probability = str(dist)
            csoc.send(str(len(code)).encode('utf-8'))
            csoc.recv(100)
            csoc.send(code.encode('utf-8'))
            csoc.recv(100)
            csoc.send(probability.encode('utf-8'))
            csoc.recv(100)
            #csoc.send(json.dumps({'content':code, 'probability':probability}).encode('utf-8'))
            #csoc.recv(len('received'))

se = search_engine(nmslib_index=search_index,
                   ref_df=code_df,
                   query2emb_func=q2emb.emb_mean)

print('MSG: successfully loaded search index')

#create server socket
ss = soc.socket(soc.AF_INET, soc.SOCK_STREAM)
ss.bind(('', int(sys.argv[1])))
print('MSG: search index server running on this machine at port#: ' + sys.argv[1])
#server loop
while(True):
	ss.listen(1)
	(csoc, caddr) = ss.accept()
	while(True):
		JSON_STRING = csoc.recv(4096)
		if(JSON_STRING == b'quit_code_359'):
			break
		query = json.loads(JSON_STRING)
		if(query.get('isValid')):
			se.search(query.get('line'), csoc, query.get('resultsReq'))
		else:
			csoc.send("invalid".encode('utf-8'))
			csoc.recv(100)
		csoc.send('end_of_query'.encode('utf-8'))
	csoc.close()
	
	
	
